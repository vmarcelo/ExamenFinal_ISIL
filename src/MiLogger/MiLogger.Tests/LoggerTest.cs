﻿using System;
using System.Configuration;
using NUnit.Framework;
using MiLogger;
using System.IO;

namespace MiLogger.Tests
{
    [TestFixture]
    public class LoggerTest
    {
        private Logger _mylogger;
        private string _mypath;

        #region Inicio
        [SetUp]
        public void Setup()
        {
            ConfigurationManager.AppSettings.Set("CarpetaDeLog", "C:\\");
            ConfigurationManager.AppSettings.Set("ConnectionString", "Data Source=.\\SQLEXPRESS;Initial Catalog=BaseDatosExamen;User ID=admin; Password=admin;Integrated Security=true");
            _mylogger = new Logger();
            _mypath = "C:\\ArchivoLog" + DateTime.Now.ToShortDateString();
        }

        [TearDown]
        public void Limpiar()
        {
            _mylogger = null;
            _mypath = null;
        }
        #endregion

        #region Log_Consola
        [Test]
        public void InsertarLog_ConsolaConNivelMensaje_Correcto()
        {
            _mylogger.LogMensaje("pruebaaaaaa", 3, 1);
        }

        [Test]
        public void InsertarLog_ConsolaConNivelAlerta_Correcto()
        {
            _mylogger.LogMensaje("pruebaaaaaa", 3, 2);
        }

        [Test]
        public void InsertarLog_ConsolaConNivelError_Correcto()
        {
            _mylogger.LogMensaje("pruebaaaaaa", 3, 3);
        }

        [Test]
        [ExpectedException(ExpectedMessage = "Configuracion Inválida de destino")]
        public void InsertarLog_ConsolaConConfiguracionInvalidadEnDestinoMensaje_Erorr()
        {
            _mylogger.LogMensaje("pruebaaaaaa", 4, 1);
        }
        #endregion

        #region Log_Archivo
        [Test]
        public void InsertarLog_ArchivoConNivelMensaje_Correcto()
        {
            CrearPath();
            _mylogger.LogMensaje("InsertarLog_ArchivoConNivelMensaje_Correcto", 2, 1);
            EliminarPath();
        }

        [Test]
        public void InsertarLog_ArchivoConNivelAlerta_Correcto()
        {
            CrearPath();
            _mylogger.LogMensaje("InsertarLog_ArchivoConNivelMensaje_Correcto", 2, 2);
            EliminarPath();
        }

        [Test]
        public void InsertarLog_ArchivoConNivelError_Correcto()
        {
            CrearPath();
            _mylogger.LogMensaje("InsertarLog_ArchivoConNivelMensaje_Correcto", 2, 3);
            EliminarPath();
        }

        [Test]
        [ExpectedException(ExpectedMessage = "Ocurrio un error")]
        public void InsertarLog_Archivo_Error()
        {
            _mylogger.LogMensaje("InsertarLog_Archivo_Error", 2, 1);
        }

        private void EliminarPath()
        {
            Directory.Delete(_mypath);
        }

        private void CrearPath()
        {
            Directory.CreateDirectory(_mypath);
        }
        #endregion

        #region Log_BaseDatos
        //[Test]
        //public void InsertarLog_BaseDatosConNivelMensaje_Correcto()
        //{
        //    _mylogger.LogMensaje("InsertarLog_ArchivoConNivelMensaje_Correcto", 1, 1);
        //}
        #endregion
    }
}
