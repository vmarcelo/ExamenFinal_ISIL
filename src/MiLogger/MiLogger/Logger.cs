﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace MiLogger
{
    public class Logger
    {   
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensajeLog">Texto del Mensaje</param>
        /// <param name="intDestinoMensaje">1 = BaseDatos, 2 = Archivo de Texto, 3 = Consola </param>
        /// <param name="intNivelError">1 = Mensaje, 2 = Alerta, 3 = Error</param>
        public void LogMensaje(string mensajeLog, int intDestinoMensaje, int intNivelError)
        {
            mensajeLog = mensajeLog.Trim();
            //validamos mensaje
            if (string.IsNullOrEmpty(mensajeLog))
            {
                return;
            }
            //validamos destino de msg
            switch (intDestinoMensaje)
            {
                case 1:
                    RegistrarLogBaseDatos(mensajeLog, intNivelError);
                    break;
                case 2:
                    RegistrarLogArchivo(mensajeLog, intNivelError);
                    break;
                case 3:
                    RegistrarLogConosola(mensajeLog, intNivelError);
                    break;
                default:
                    throw new Exception("Configuracion Inválida de destino");
            }
        }

        private static void RegistrarLogBaseDatos(string mensajeLog, int intNivelError)
        {
            SqlConnection connection =
                new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            connection.Open();
            SqlCommand command;
            switch (intNivelError)
            {
                case 1:
                     command = new SqlCommand("Insert into Log Values('" + mensajeLog + "', " + intNivelError.ToString() + ")");
                    break;
                case 2:
                     command = new SqlCommand("Insert into Log Values('" + mensajeLog + "', " + intNivelError.ToString() + ")");
                    break;
                case 3:
                     command = new SqlCommand("Insert into Log Values('" + mensajeLog + "', " + intNivelError.ToString() + ")");
                    break;
                default:
                    throw new Exception("Configuracion Inválida de nivel");
            }

            command.ExecuteNonQuery();
        }

        private static void RegistrarLogArchivo(string mensajeLog, int intNivelError)
        {
            try
            {
                var strMsg = string.Empty;
                if (File.Exists(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" +
                                DateTime.Now.ToShortDateString() + ".txt"))
                {
                    strMsg =
                        File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] +
                                         "ArchivoLog" +
                                         DateTime.Now.ToShortDateString() + ".txt");
                }

                switch (intNivelError)
                {
                    case 1:
                        strMsg = strMsg + DateTime.Now.ToShortDateString() + mensajeLog + "', " + intNivelError.ToString();
                        break;
                    case 2:
                        strMsg = strMsg + DateTime.Now.ToShortDateString() + mensajeLog + "', " + intNivelError.ToString();
                        break;
                    case 3:
                        strMsg = strMsg + DateTime.Now.ToShortDateString() + mensajeLog + "', " + intNivelError.ToString();
                        break;
                    default:
                        throw new Exception("Configuracion Inválida de nivel");
                }

                File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" +
                                  DateTime.Now.ToShortDateString() + ".txt", strMsg);
            }
            catch (Exception)
            {
                throw new Exception("Ocurrio un error");
            }
        }

        private static void RegistrarLogConosola(string mensajeLog, int intNivelError)
        {
            switch (intNivelError)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                default:
                    throw new Exception("Configuracion Inválida de nivel");
            }
        }
    }
}